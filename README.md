# Calculator API Web Service

CalculatorAPI is a simple calculator web service which provides the results of an addition, substraction, multiplication or division of two numbers. 

It also provides the history of all calculations made.

## Prerequisites

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Endpoints - Calculation

- Get the result of two number addition
```shell
GET /add?firstNumber={first_number}&secondNumber={second_number}
```

- Get the result of two number substraction
```shell
GET /substract?firstNumber={first_number}&secondNumber={second_number}
```

- Get the result of two number multiplication
```shell
GET /multiply?firstNumber={first_number}&secondNumber={second_number}
```

- Get the result of two number division
```shell
GET /divide?firstNumber={first_number}&secondNumber={second_number}
```

## Endpoints - History

- Get the complete calculations history
```shell
GET /history
```

- Get the complete calculations history for a specific date (format: dd-MM-yyyy)
```shell
GET /history-date?date={date}
```

- Get the complete calculations history between two dates (format: dd-MM-yyyy)
```shell
GET /history-period?start={start_date}&end={end_date}
```
