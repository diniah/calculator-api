package com.sgx.calculator;

import com.sgx.calculator.service.AdditionService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AdditionServiceTests {

    private AdditionService additionService = new AdditionService();

    @Test
    public void twoPositiveNumberAdditionResultsPositiveNumber() {
        Double result = this.additionService.calculate(5.0, 5.0);
        assertEquals(10, result);
    }

    @Test
    public void twoNegativeNumberAdditionResultsNegativeNumber() {
        Double result = this.additionService.calculate(-5.0, -5.0);
        assertEquals(-10, result);
    }
}
