package com.sgx.calculator;

import com.sgx.calculator.service.SubstractionService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SubstractionServiceTests {
    private SubstractionService substractionService = new SubstractionService();

    @Test
    public void twoNumbersSubstractionResult() {
        Double result = this.substractionService.calculate(10.0, 5.0);
        assertEquals(5, result);
    }
}
