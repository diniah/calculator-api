package com.sgx.calculator;

import com.sgx.calculator.service.MultiplicationService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MultiplicationServiceTests {

    private MultiplicationService multiplicationService = new MultiplicationService();

    @Test
    public void twoPositiveNumberMultiplicationResultsPositiveNumber() {
        Double result = this.multiplicationService.calculate(2.0, 3.0);
        assertEquals(6, result);
    }

    @Test
    public void twoNegativeNumberMultiplicationResultsPositiveNumber() {
        Double result = this.multiplicationService.calculate(-5.0, -5.0);
        assertEquals(25, result);
    }

    @Test
    public void positiveAndNegativeNumberMultiplicationResultsNegativeNumber() {
        Double result = this.multiplicationService.calculate(-2.0, 5.0);
        assertEquals(-10, result);
    }
}
