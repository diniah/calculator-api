package com.sgx.calculator;

import com.sgx.calculator.exception.DivideByZeroException;
import com.sgx.calculator.service.DivisionService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DivisionServiceTests {

    private DivisionService divisionService = new DivisionService();

    @Test
    public void twoPositiveNumberDivisionResultsPositiveNumber() {
        Double result = this.divisionService.calculate(10.0, 2.0);
        assertEquals(5, result);
    }

    @Test
    public void twoNegativeNumberDivisionResultsPositiveNumber() {
        Double result = this.divisionService.calculate(-5.0, -5.0);
        assertEquals(1, result);
    }

    @Test
    public void positiveAndNegativeNumberDivisionResultsNegativeNumber() {
        Double result = this.divisionService.calculate(-10.0, 5.0);
        assertEquals(-2, result);
    }

    @Test
    public void dividedByZeroDivisionResultsException() {
        assertThrows(DivideByZeroException.class, () -> {
            this.divisionService.calculate(10.0, 0.0);
        });
    }
}
