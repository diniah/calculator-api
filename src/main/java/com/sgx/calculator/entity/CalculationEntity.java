package com.sgx.calculator.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "calculation")
public class CalculationEntity {
    @Id
    @GeneratedValue
    private int id;

    @Temporal(TemporalType.DATE)
    private Date dateCalculation;
    private String type;
    private Double firstNumber;
    private Double secondNumber;
    private Double result;

    public CalculationEntity() {

    }

    public CalculationEntity(Date dateCalculation, String type, Double firstNumber, Double secondNumber, Double result) {
        this.dateCalculation = dateCalculation;
        this.type = type;
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.result = result;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateCalculation() {
        return dateCalculation;
    }

    public void setDateCalculation(Date dateCalculation) {
        this.dateCalculation = dateCalculation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(Double firstNumber) {
        this.firstNumber = firstNumber;
    }

    public Double getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(Double secondNumber) {
        this.secondNumber = secondNumber;
    }

    public Double getResult() {
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }
}
