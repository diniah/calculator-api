package com.sgx.calculator.controller;

import com.sgx.calculator.dto.CalculationDTO;
import com.sgx.calculator.entity.CalculationEntity;
import com.sgx.calculator.enums.CalculationTypeEnum;
import com.sgx.calculator.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class CalculationController {
    private AdditionService additionService;
    private SubstractionService substractionService;
    private MultiplicationService multiplicationService;
    private DivisionService divisionService;
    private DatabaseOperationsService databaseOperationsService;

    private static final Logger LOGGER = LogManager.getLogger(CalculationController.class);

    public CalculationController(AdditionService additionService,
                                 SubstractionService substractionService,
                                 MultiplicationService multiplicationService,
                                 DivisionService divisionService,
                                 DatabaseOperationsService databaseOperationsService) {
        this.additionService = additionService;
        this.substractionService = substractionService;
        this.multiplicationService = multiplicationService;
        this.divisionService = divisionService;
        this.databaseOperationsService = databaseOperationsService;
    }

    @GetMapping("/add")
    public Double add(@RequestParam Double firstNumber, @RequestParam Double secondNumber) {
        LOGGER.info("Addition with parameters " + firstNumber + " and " + secondNumber);

        Double result = this.additionService.calculate(firstNumber, secondNumber);
        CalculationEntity calculationEntity = new CalculationEntity(new Date(), CalculationTypeEnum.ADDITION.getValue(), firstNumber, secondNumber, result);
        this.databaseOperationsService.save(calculationEntity);

        return result;
    }

    @GetMapping("/substract")
    public Double substract(@RequestParam Double firstNumber, @RequestParam Double secondNumber) {
        LOGGER.info("Substraction with parameters " + firstNumber + " and " + secondNumber);

        Double result = this.substractionService.calculate(firstNumber, secondNumber);
        CalculationEntity calculationEntity = new CalculationEntity(new Date(), CalculationTypeEnum.SUBSTRACTION.getValue(), firstNumber, secondNumber, result);
        this.databaseOperationsService.save(calculationEntity);

        return result;
    }

    @GetMapping("/multiply")
    public Double multiply(@RequestParam Double firstNumber, @RequestParam Double secondNumber) {
        LOGGER.info("Multiplication with parameters " + firstNumber + " and " + secondNumber);

        Double result = this.multiplicationService.calculate(firstNumber, secondNumber);
        CalculationEntity calculationEntity = new CalculationEntity(new Date(), CalculationTypeEnum.MULTIPLICATION.getValue(), firstNumber, secondNumber, result);
        this.databaseOperationsService.save(calculationEntity);

        return result;
    }

    @GetMapping("/divide")
    public Double divide(@RequestParam Double firstNumber, @RequestParam Double secondNumber) {
        LOGGER.info("Division with parameters " + firstNumber + " and " + secondNumber);

        Double result = this.divisionService.calculate(firstNumber, secondNumber);
        CalculationEntity calculationEntity = new CalculationEntity(new Date(), CalculationTypeEnum.DIVISION.getValue(), firstNumber, secondNumber, result);
        this.databaseOperationsService.save(calculationEntity);

        return result;
    }

    @GetMapping("/history")
    public List<CalculationDTO> getHistory() {
        LOGGER.info("Get the complete calculations history");
        return this.databaseOperationsService.getHistory();
    }

    @GetMapping("/history-date")
    public List<CalculationDTO> getHistoryDate(@RequestParam(value = "date") @DateTimeFormat(pattern = "dd-MM-yyyy") Date dateHistory) {
        LOGGER.info("Get the complete calculations history for the specific date " + dateHistory);
        return this.databaseOperationsService.getHistoryDate(dateHistory);
    }

    @GetMapping("/history-period")
    public List<CalculationDTO> getHistoryPeriod(@RequestParam(value = "start") @DateTimeFormat(pattern = "dd-MM-yyyy") Date startDate,
                                                 @RequestParam(value = "end") @DateTimeFormat(pattern = "dd-MM-yyyy") Date endDate) {
        LOGGER.info("Get the complete calculations history between " + startDate + " and " + endDate);
        return this.databaseOperationsService.getHistoryPeriod(startDate, endDate);
    }
}
