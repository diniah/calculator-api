package com.sgx.calculator.mapper;

import com.sgx.calculator.dto.CalculationDTO;
import com.sgx.calculator.entity.CalculationEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CalculationMapper {
    CalculationDTO calculationEntityToCalculationDTO(CalculationEntity calculationEntity);

    List<CalculationDTO> calculationEntityListToCalculationDTOList(List<CalculationEntity> calculationEntity);
}
