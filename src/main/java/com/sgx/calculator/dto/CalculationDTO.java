package com.sgx.calculator.dto;

import java.util.Date;

public class CalculationDTO {
    private int id;
    private Date dateCalculation;
    private String type;
    private Double firstNumber;
    private Double secondNumber;
    private Double result;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateCalculation() {
        return dateCalculation;
    }

    public void setDateCalculation(Date dateCalculation) {
        this.dateCalculation = dateCalculation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(Double firstNumber) {
        this.firstNumber = firstNumber;
    }

    public Double getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(Double secondNumber) {
        this.secondNumber = secondNumber;
    }

    public Double getResult() {
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "Date : " + dateCalculation +
            " | Type : " + type +
            " | First number : " + firstNumber +
            " | Second number : " + secondNumber +
            " | Result : " + result;
    }
}
