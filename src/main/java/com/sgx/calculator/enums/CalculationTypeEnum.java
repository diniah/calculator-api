package com.sgx.calculator.enums;

public enum CalculationTypeEnum {
    ADDITION("ADD"),
    SUBSTRACTION("SUB"),
    MULTIPLICATION("MUL"),
    DIVISION("DIV");

    private final String value;

    CalculationTypeEnum(String type) {
        this.value = type;
    }

    public String getValue() {
        return value;
    }
}
