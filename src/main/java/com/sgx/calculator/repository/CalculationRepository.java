package com.sgx.calculator.repository;

import com.sgx.calculator.entity.CalculationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface CalculationRepository extends CrudRepository<CalculationEntity, Integer> {
    Iterable<CalculationEntity> findByDateCalculation(Date date);

    Iterable<CalculationEntity> findByDateCalculationBetween(Date dateStart, Date dateEnd);
}
