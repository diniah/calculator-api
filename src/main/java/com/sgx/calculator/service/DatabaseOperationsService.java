package com.sgx.calculator.service;

import com.sgx.calculator.dto.CalculationDTO;
import com.sgx.calculator.entity.CalculationEntity;
import com.sgx.calculator.exception.StartDateGreaterThanEndDateException;
import com.sgx.calculator.mapper.CalculationMapper;
import com.sgx.calculator.repository.CalculationRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class DatabaseOperationsService {

    private CalculationRepository calculationRepository;
    private CalculationMapper calculationMapper;

    public DatabaseOperationsService(CalculationRepository calculationRepository, CalculationMapper calculationMapper) {
        this.calculationRepository = calculationRepository;
        this.calculationMapper = calculationMapper;
    }

    public void save(CalculationEntity calculationEntity) {
        this.calculationRepository.save(calculationEntity);
    }

    public List<CalculationDTO> getHistory() {
        return createCalculationDTOListFromCalculationEntityList(this.calculationRepository.findAll());
    }

    public List<CalculationDTO> getHistoryDate(Date date) {
        return createCalculationDTOListFromCalculationEntityList(this.calculationRepository.findByDateCalculation(date));
    }

    public List<CalculationDTO> getHistoryPeriod(Date startDate, Date endDate) {
        if (startDate.after(endDate)) {
            throw new StartDateGreaterThanEndDateException("Error: the start date is after the end date");
        }
        return createCalculationDTOListFromCalculationEntityList(this.calculationRepository.findByDateCalculationBetween(startDate, endDate));
    }

    private List<CalculationDTO> createCalculationDTOListFromCalculationEntityList(Iterable<CalculationEntity> calculationEntityList) {
        List<CalculationEntity> list = new ArrayList<>();

        for (CalculationEntity elt : calculationEntityList) {
            list.add(elt);
        }
        return this.calculationMapper.calculationEntityListToCalculationDTOList(list);
    }
}
