package com.sgx.calculator.service;

import com.sgx.calculator.exception.DivideByZeroException;
import org.springframework.stereotype.Service;

@Service
public class DivisionService extends CalculationService {

    @Override
    public Double calculate(Double firstNumber, Double secondNumber) {
        if (secondNumber == 0) {
            throw new DivideByZeroException("Error: division by zero not allowed");
        }
        return firstNumber / secondNumber;
    }
}
