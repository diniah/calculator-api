package com.sgx.calculator.service;

import org.springframework.stereotype.Service;

@Service
public class SubstractionService extends CalculationService {

    @Override
    public Double calculate(Double firstNumber, Double secondNumber) {
        return firstNumber - secondNumber;
    }
}
