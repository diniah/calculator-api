package com.sgx.calculator.service;

public abstract class CalculationService {
    public abstract Double calculate(Double firstNumber, Double secondNumber);
}
