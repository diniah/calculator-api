package com.sgx.calculator.exception;

public class StartDateGreaterThanEndDateException extends RuntimeException {
    public StartDateGreaterThanEndDateException(String message) {
        super(message);
    }
}
